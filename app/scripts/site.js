/**
 * @name Site
 * @description Global variables and functions
 * @version 1.0
 */

var Site = (function($, window, undefined) {
  'use strict';

  var privateMethod = function() {
    $('.menu-mb').on('click',function(){
      if($('.nav-bar .mb').css('display')=="none"){
        $('.nav-bar .mb').fadeIn('slow');
        $(this).toggleClass('active')
      }
      else
      {
        $('.nav-bar .mb').fadeOut('slow');
        $(this).toggleClass('active')
      }
    })
  };

  return {
    publicMethod: privateMethod
  };

})(jQuery, window);

jQuery(function() {
  Site.publicMethod();
});
